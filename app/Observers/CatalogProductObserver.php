<?php

namespace App\Observers;

use App\Models\CatalogProduct;

class CatalogProductObserver
{
    public function creating(CatalogProduct $product)
    {
        $maxPriority = CatalogProduct::where('category_id', $product->category_id)->max('priority');
        $product->priority = $maxPriority + 10;
    }
}