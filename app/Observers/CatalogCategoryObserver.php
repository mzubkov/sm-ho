<?php

namespace App\Observers;

use App\Models\CatalogCategory;

class CatalogCategoryObserver
{
    public function creating(CatalogCategory $category)
    {
        $maxPriority = CatalogCategory::max('priority');
        $category->priority = $maxPriority + 10;
    }
}