<?php

namespace App\Widgets;

use App\Models\CatalogCategory;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Request;

class Sidebar extends AbstractWidget
{
    protected $activeClass = 'active';
    protected $current = 'current-page';

    protected $menu = [
        'settings' => ['title' => 'Настройки', 'url' => '/admin/settings', 'icon' => 'edit', 'class' => ''],
        'price-list' => ['title' => 'Прайс-лист', 'url' => '/admin/price-list', 'icon' => 'list', 'class' => ''],
    ];

    public function run()
    {
        $catalog = CatalogCategory::all();
        foreach ($catalog as $item) {
            $this->menu['price-list']['items'][] = [
                'title' => $item->name,
                'url' => '/admin/catalog_category?id=' . $item->id,
                'icon' => ''
            ];
        }

        $this->decorate();

        return view("widgets.sidebar", [
            'menu' => $this->menu,
        ]);
    }

    protected function decorate()
    {
        $url = '/' . Request::fullUrl();
        foreach ($this->menu as &$v) {
            if (strpos($url, $v['url']) !== false) {
                $v['class'] = $this->activeClass;
            }

            if (isset($v['items'])) {
                foreach ($v['items'] as &$subItem) {
                    if (strpos($url, $subItem['url']) !== false) {
                        $subItem['class'] = $this->current;
                        $v['class'] = $this->activeClass;
                    }
                }
            }
        }
    }
}