<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PriceList extends Model
{
    const
        STORAGE_PATH = 'upload/price_list'
    ;

    protected $table = 'price_list';

    static function getFilePath()
    {
        return storage_path(self::STORAGE_PATH);
    }

    public function getFile()
    {
        return self::getFilePath() . '/' . $this->name;
    }
}
