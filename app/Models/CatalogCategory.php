<?php

namespace App\Models;

use App\Models\Traits\Ordered;

class CatalogCategory extends BaseModel
{
    use Ordered;

    protected $table = 'catalog_category';

    public static $sortOrder = ['priority' => 'asc'];

    public function products()
    {
        return $this->hasMany('App\Models\CatalogProduct', 'category_id', 'id');
    }
}
