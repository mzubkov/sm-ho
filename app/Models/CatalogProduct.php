<?php

namespace App\Models;

use App\Models\Traits\Ordered;

class CatalogProduct extends BaseModel
{
    use Ordered;

    protected $table = 'catalog_product';

    public static $sortOrder = ['priority' => 'asc'];

    public function category()
    {
        return $this->belongsTo('App\Models\CatalogCategory', 'category_id', 'id');
    }
}
