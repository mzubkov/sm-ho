<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    public function toggle()
    {
        $this->is_published = ($this->is_published) ? 0 : 1;
        $this->save();
    }
}
