<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Auth;

Route::get('/', 'Controller@index');
Route::get('/download', ['as' => 'price.download', 'uses' => 'Controller@download']);
Route::get('/login', 'Auth\AuthController@getLogin');
Route::post('/login', 'Auth\AuthController@postLogin');
Route::get('/logout', 'Controller@logout');

Route::group([
        'middleware' => 'auth',
        'prefix' => 'admin',
        'namespace' => 'Admin',
    ],
    function() {
        Route::get('/', 'AdminController@index');

        //Route::resource('settings', 'AdminSettingsController', ['only' => ['index', 'edit', 'update']]);
        Route::controller('settings', 'AdminSettingsController', [
            'getIndex' => 'admin.settings.index',
            'getEdit' => 'admin.settings.edit',
            'postUpdate' => 'admin.settings.update'
        ]);

        Route::controller('price-list', 'AdminPriceListController', [
            'getIndex' => 'admin.priceList.index',
            'getUpload' => 'admin.priceList.upload',
            'postUpload' => 'admin.priceList.uploadFile',
        ]);

        Route::controller('catalog_category', 'AdminCatalogCategoryController', [
            'getIndex' => 'admin.catalog_category.index',
            'getCreate' => 'admin.catalog_category.create',
            'getEdit' => 'admin.catalog_category.edit',
            'postSave' => 'admin.catalog_category.save',
            'postToggle' => 'admin.catalog_category.toggle',
            'postPriority' => 'admin.catalog_category.priority',
            'postDelete' => 'admin.catalog_category.delete',
        ]);

        Route::controller('catalog_product', 'AdminCatalogProductController', [
            'getIndex' => 'admin.catalog_product.index',
            'getCreate' => 'admin.catalog_product.create',
            'getEdit' => 'admin.catalog_product.edit',
            'postSave' => 'admin.catalog_product.save',
            'postToggle' => 'admin.catalog_product.toggle',
            'postPriority' => 'admin.catalog_product.priority',
            'postDelete' => 'admin.catalog_product.delete',
        ]);
    }
);