<?php

namespace App\Http\Controllers;

use App\Models\CatalogCategory;
use App\Models\PriceList;
use App\Models\Settings;
use Illuminate\Routing\Controller as BaseController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Controller extends BaseController
{
    public function index()
    {
        $settings = Settings::all()->keyBy('identifier')->toArray();
        return view('index', [
            'title' => $settings['site_title']['value'],
            'keywords' => '',
            'description' => '',
            'phone' => $settings['phone']['value'],
            'email' => $settings['email']['value'],
            'price_list' => $this->getPriceList()
        ]);
    }

    public function download()
    {
        if (!$price = PriceList::orderBy('id', 'desc')->first()) {
            throw new NotFoundHttpException;
        }

        return response()->download($price->getFile());
    }

    protected function getPriceList()
    {
        return CatalogCategory::where(['is_published' => 1])->get();
    }
}
