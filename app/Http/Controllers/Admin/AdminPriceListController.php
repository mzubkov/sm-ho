<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PriceUploadRequest;
use App\Models\CatalogCategory;
use App\Models\PriceList;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Session;

class AdminPriceListController extends BaseController
{
    use ValidatesRequests;

    public function getIndex()
    {
        return view('admin_price_list/index', [
            'title' => 'Прайс-лист',
            'items' => CatalogCategory::all(),
            'pricelist' => PriceList::orderBy('id', 'desc')->first()
        ]);
    }

    public function getUpload()
    {
        return view('admin_price_list/upload', [
            'title' => 'Загрущка прайс-листа',
            'items' => CatalogCategory::all()
        ]);
    }

    public function postUpload(PriceUploadRequest $request)
    {
        $this->validate($request, []);

        $file = $request->file('file');
        $file->move(PriceList::getFilePath(), $file->getClientOriginalName());
        $price = new PriceList();
        $price->name = $file->getClientOriginalName();
        $price->save();

        Session::flash('flashMessage', 'Файл успешно загружен');
        return redirect(route('admin.priceList.index'));
    }

}