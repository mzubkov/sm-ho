<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CatalogProductRequest;
use App\Models\CatalogCategory;
use App\Models\CatalogProduct;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Http\Request;

class AdminCatalogProductController extends BaseController
{
    use ValidatesRequests;


    public function getCreate($id)
    {
        if (!$category = CatalogCategory::find($id)) {
            throw new NotFoundHttpException;
        }

        return view('admin_catalog_product/create', [
            'title' => 'Создание категории',
            'category' => $category,
            'item' => new CatalogProduct()
        ]);
    }

    public function getEdit($id)
    {
        $item = CatalogProduct::find($id);
        return view('admin_catalog_product/edit', [
            'title' => 'Редактирование категории',
            'category' => $item->category,
            'item' => $item
        ]);
    }

    public function postSave(CatalogProductRequest $request)
    {
        $this->validate($request, []);

        if (func_num_args() == 1) {
            $item = new CatalogProduct();
        } else {
            $item = CatalogProduct::find(func_get_arg(1));
        }

        $item->name = $request->name;
        $item->price = $request->price;
        $item->category_id = $request->category_id;
        $item->is_published = (int) $request->is_published;
        $item->save();

        return redirect(route('admin.catalog_category.index', ['id' => $item->category_id]));
    }

    public function postToggle($id)
    {
        /** @var CatalogProduct $item */
        if (!$item = CatalogProduct::find($id)) {
            Session::flash('flashError', 'Запись не найдена');
            return;
        } else {
            try {
                $item->toggle();
            } catch (\Exception $e) {
                Session::flash('flashError', 'Произошла ошибка при удалении записи');
            }
        }
    }

    public function postPriority(Request $request)
    {
        $models = CatalogProduct::where('category_id', $request->category_id)->get()->keyBy('id');
        $priority = CatalogProduct::where('category_id', $request->category_id)->min('priority');
        foreach ($request->ids as $id) {
            $model = $models[$id];
            $model->priority = $priority;
            $model->save();
            $priority += 10;
        }
    }

    public function postDelete($id)
    {
        /** @var Model $item */
        if (!$item = CatalogProduct::find($id)) {
            Session::flash('flashError', 'Запись не найдена');
            return;
        } else {
            try {
                $item->delete();
                Session::flash('flashMessage', 'Запись удалена');
            } catch (\Exception $e) {
                Session::flash('flashError', 'Произошла ошибка при удалении записи');
            }
        }
    }
}