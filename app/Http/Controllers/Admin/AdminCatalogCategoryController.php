<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CatalogCategoryRequest;
use App\Models\CatalogCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AdminCatalogCategoryController extends BaseController
{
    use ValidatesRequests;

    public function getIndex(\Illuminate\Http\Request $request)
    {
        /** @var CatalogCategory $category */
        if (!$category = CatalogCategory::find($request->query('id'))) {
            throw new NotFoundHttpException;
        }

        return view('admin_catalog_category/index', [
            'title' => $category->name . '. Продукция',
            'item' => $category,
            'items' => $category->products
        ]);
    }

    public function getCreate()
    {
        return view('admin_catalog_category/create', [
            'title' => 'Создание категории',
            'item' => new CatalogCategory()
        ]);
    }

    public function getEdit($id)
    {
        $item = CatalogCategory::find($id);
        return view('admin_catalog_category/edit', [
            'title' => 'Редактирование категории',
            'item' => $item
        ]);
    }

    public function postSave(CatalogCategoryRequest $request)
    {
        $this->validate($request, []);

        if (func_num_args() == 1) {
            $item = new CatalogCategory();
        } else {
            $item = CatalogCategory::find(func_get_arg(1));
        }

        $item->name = $request->name;
        $item->is_published = (int) $request->is_published;
        $item->save();

        return redirect(route('admin.priceList.index'));
    }

    public function postToggle($id)
    {
        /** @var CatalogCategory $item */
        if (!$item = CatalogCategory::find($id)) {
            Session::flash('flashError', 'Запись не найдена');
            return;
        } else {
            try {
                $item->toggle();
            } catch (\Exception $e) {
                Session::flash('flashError', 'Произошла ошибка при удалении записи');
            }
        }
    }

    public function postPriority(Request $request)
    {
        $models = CatalogCategory::find($request->ids)->keyBy('id');
        $priority = CatalogCategory::find($request->ids)->min('priority');
        foreach ($request->ids as $id) {
            $model = $models[$id];
            $model->priority = $priority;
            $model->save();
            $priority += 10;
        }
    }

    public function postDelete($id)
    {
        /** @var Model $item */
        if (!$item = CatalogCategory::find($id)) {
            Session::flash('flashError', 'Запись не найдена');
            return;
        } else {
            try {
                $item->delete();
                Session::flash('flashMessage', 'Запись удалена');
            } catch (\Exception $e) {
                Session::flash('flashError', 'Произошла ошибка при удалении записи');
            }
        }
    }
}