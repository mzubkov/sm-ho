<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SettingsUpdateRequest;
use App\Models\Settings;
use Hamcrest\Core\Set;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class AdminSettingsController extends BaseController
{
    use ValidatesRequests;

    public function getIndex()
    {
        return view('admin_settings/index', [
            'title' => 'Настройки',
            'settings' => Settings::all()
        ]);
    }

    public function getEdit($id)
    {
        $item = Settings::find($id);
        return view('admin_settings/edit', [
            'title' => 'Настройки. Редактирование',
            'item' => $item
        ]);
    }

    public function postUpdate($id, SettingsUpdateRequest $request)
    {
        $this->validate($request, []);

        $item = Settings::find($id);
        $item->value = $request->value;
        $item->save();

        return redirect(route('admin.settings.edit', ['settings' => $id]));
    }
}