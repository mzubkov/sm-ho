<div class="row">
    @if(count($errors) > 0)
        <div class="col-md-3 col-sm-3"></div>
        <div class="alert alert-danger col-md-6 col-sm-6 col-xs-12">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <div class="col-md-3 col-sm-3"></div>
    @endif
</div>