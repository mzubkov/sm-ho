<!DOCTYPE html>
<html lang="en">

@include('layouts.admin.head')

<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            @widget('Sidebar')

            @include('layouts.admin.top_nav')

            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>@yield('page_title')</h3>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                        @yield('content')
                    </div>
                </div>

                @include('layouts.admin.footer')

            </div>
        </div>

    </div>

    @include('layouts.admin.bottom_js')
</body>

</html>