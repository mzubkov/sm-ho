<script src="/cp/js/bootstrap.min.js"></script>
<script src="/cp/js/nprogress.js"></script>
<!-- bootstrap progress js -->
<script src="/cp/js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="/cp/js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="/cp/js/icheck/icheck.min.js"></script>
<!-- Switchery -->
<script src="/cp/js/switchery/switchery.min.js"></script>

<script src="/cp/js/custom.js"></script>

<!-- PNotify -->
<script src="/cp/js/notify/pnotify.min.js"></script>
<script src="/cp/js/notify/pnotify.confirm.js"></script>

@section('js_include')

@show

<script type="text/javascript">
    $('document').ready(function(){
        @if($msg = Illuminate\Support\Facades\Session::get('flashMessage', false))
            new PNotify({
                title: 'Сообщение',
                text: '{{ $msg }}',
                type: 'success',
                styling: 'bootstrap3'
            });
        @endif

        @if($errMsg = Illuminate\Support\Facades\Session::get('flashError', false))
            new PNotify({
                title: 'Сообщение',
                text: '{{ $errMsg }}',
                type: 'error',
                styling: 'bootstrap3'
            });
        @endif
    });
</script>