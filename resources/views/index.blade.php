@extends('layouts.main')

@section('title', $title)

@section('content')
<div class="container">
    <div class="header row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="text-center">
                <a href="/" title="{{ $title }}">
                    <img src="/images/logo01.jpg" alt="{{ $title }}" class="img-responsive">
                </a>
            </div>

            <div class="text-center contacts">
                {{ $phone }} | {{ $email }}
            </div>

            <div class="text-center">
                <a href="{{ route('price.download') }}" class="download">Скачать полный прайс-лист</a>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered price-list">
                <thead>
                    <th width="80%">Номенклатура / характеристика</th>
                    <th class="text-center">Цена, руб.</th>
                </thead>
                <tbody>
                @foreach($price_list as $category)
                    <tr class="category">
                        <td colspan="2">{{ $category->name }}</td>
                    </tr>
                    @foreach($category->products()->where(['is_published' => 1])->get() as $item)
                        <tr class="item">
                            <td>{{ $item->name }}</td>
                            <td class="text-center">{{ $item->price }}</td>
                        </tr>
                    @endforeach
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

</div>
@endsection