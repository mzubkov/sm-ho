<div class="x_content">
    <br>

    @include('_include.form_errors')

    {!! Form::open(['url' => route('admin.catalog_product.save', ['product' => $item]), 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
    {!! Form::model($item) !!}
    {!! Form::hidden('category_id', $category->id) !!}
    <div class="form-group">
        {!! Form::label('name', 'Название', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::text('name', null, ['class' => 'form-control col-md-7 col-xs-12']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('price', 'Цена', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::text('price', null, ['class' => 'form-control col-md-7 col-xs-12']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('is_published', 'Активность', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::checkbox('is_published', 1, null, ['class' => 'js-switch']) !!}
        </div>
    </div>

    <div class="ln_solid"></div>
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <a href="{{ route('admin.catalog_category.index', ['id' => $category->id]) }}" type="submit" class="btn btn-primary" >Отмена</a>
            <button type="submit" class="btn btn-success">Сохранить</button>
        </div>
    </div>
    {!! Form::close() !!}
</div>