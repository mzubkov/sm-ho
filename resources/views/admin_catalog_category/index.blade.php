@extends('layouts.admin')

@section('title', $title)
@section('page_title', $title)

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel" style="height:600px;">
            <a href="{{ route('admin.catalog_product.create', ['category_id' => $item->id]) }}" type="button" class="btn btn-sm btn-success">Добавить продукт</a>

            @include('admin_catalog_product.include.items_list')
        </div>
    </div>
@endsection