<div class="x_content">
    <form>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <table class="table table-striped jambo_table bulk_action table-sortable">
            <thead>
            <tr class="headings">
                <th></th>
                <th class="column-title" width="90%">Название </th>
                <th class="column-title">Действия </th>
            </tr>
            </thead>

            <tbody>
            @forelse($items as $item)
                <tr class="">
                    <td>
                        <i class="icon-move fa fa-arrows-v"></i>
                        <input type="hidden" name="ids[]" value="{{ $item->id }}" class="item_id">
                    </td>
                    <td class=" ">
                        <a href="{{ route('admin.catalog_category.index', ['id' => $item->id]) }}">{{ $item->name }}</a>
                    </td>
                    <td class="actions last">
                        <a href="{{ route('admin.catalog_category.index', ['id' => $item->id]) }}" class="btn btn-xs btn-info btn-action" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Список товаров"><i class="fa fa-list"></i></a>
                        <? if ($item->is_published) { ?>
                            <a href="{{ route('admin.catalog_category.toggle', ['id' => $item->id]) }}" class="btn btn-xs btn-unpublish btn-toggle" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Снять с публикации"><i class="fa fa-lightbulb-o"></i></a>
                        <? } else { ?>
                            <a href="{{ route('admin.catalog_category.toggle', ['id' => $item->id]) }}" class="btn btn-xs btn-publish btn-toggle" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Опубликовать"><i class="fa fa-lightbulb-o"></i></a>
                        <? }?>
                        <a href="{{ route('admin.catalog_category.edit', ['id' => $item->id]) }}" class="btn btn-xs btn-edit" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Редактировать"><i class="fa fa-edit"></i></a>
                        <a href="{{ route('admin.catalog_category.delete', ['id' => $item->id]) }}"
                           class="btn btn-xs btn-delete"
                           type="button"
                           data-placement="top"
                           data-toggle="tooltip"
                           data-original-title="Удалить"
                        ><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="2">Список пуст</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </form>
</div>

@section('css_include')
    @parent
    <style>
        body.dragging, body.dragging * {
            cursor: move !important;
        }

        .dragged {
            position: absolute;
            opacity: 0.5;
            z-index: 2000;
        }

        .icon-move {
            cursor: pointer;
        }
    </style>
@show

@section('js_include')
    @parent
    <script src="/vendors/jquery-sortable/jquery-sortable.js"></script>
@show

<script type="text/javascript">
    function deleteNode(el) {
        $.post($(el).attr('href'), {'_token': '{{ csrf_token() }}' })
            .done(function(){
                window.location.reload();
            });
    }

    function deleteNotification(callback, el) {
        (new PNotify({
            title: 'Удаление записи',
            text: 'Вы действительно хотите удалить запись?',
            icon: 'glyphicon glyphicon-question-sign',
            hide: false,
            type: "info",
            confirm: {
                confirm: true,
                buttons: [
                    {
                        text: "Отмена",
                        addClass: "btn-primary",
                        click: function(notice){
                            notice.remove();
                            $('.ui-pnotify-modal-overlay').remove();
                            notice.get().trigger("pnotify.cancel", notice);
                        }
                    },
                    {
                        text: "Удалить",
                        addClass: "btn-danger",
                        click: function(notice, value){
                            notice.remove();
                            $('.ui-pnotify-modal-overlay').remove();
                            notice.get().trigger("pnotify.confirm", [notice, value]);
                        }
                    }
                ],
            },
            history: {
                history: false
            },
            addclass: 'stack-modal dark',
            styling: 'bootstrap3',
            stack: {
                'dir1': 'down',
                'dir2': 'right',
                'modal': true
            }
        }))
        .get().on('pnotify.confirm', function() {
            callback(el);
        }).on('pnotify.cancel', function() {

        });

        return false;
    }

    $('document').ready(function(){
        $(".table-sortable").sortable({
            containerSelector: '.table-sortable',
            itemPath: '> tbody',
            itemSelector: 'tr',
            handle: 'i.icon-move',
            onDragStart: function ($item, container, _super) {
                // Duplicate items of the no drop area
                if(!container.options.drop)
                    $item.clone().insertAfter($item);
                _super($item, container);
            },
            onDrop: function ($item, container, _super, event) {
                $item.removeClass(container.group.options.draggedClass).removeAttr("style");
                $("body").removeClass(container.group.options.bodyClass);

                $.post('{{ route('admin.catalog_category.priority') }}', $(container.el).parent('form').serialize())
                    .done(function(){
                        window.location.reload();
                    });
            }
        });

        $('.btn-delete').click(function(){
            deleteNotification(deleteNode, this);
            return false;
        });

        $('.btn-toggle').click(function(){
            $.post($(this).attr('href'), {'_token': '{{ csrf_token() }}' })
                .done(function(){
                    window.location.reload();
                });
            return false;
        });
    });
</script>
