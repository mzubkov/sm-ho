@extends('layouts.admin')

@section('title', $title)
@section('page_title', $title)

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel" style="height:600px;">
            <div class="x_content">
                <br>

                @include('_include.form_errors')

                {!! Form::open(['url' => route('admin.priceList.uploadFile'), 'files' => 'true', 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                {!! Form::token() !!}
                <div class="form-group">
                    {!! Form::label('file', 'Выерите файл', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::file('file') !!}
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <a href="{{ route('admin.priceList.index') }}" type="submit" class="btn btn-primary" >Отмена</a>
                        <button type="submit" class="btn btn-success">Сохранить</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection