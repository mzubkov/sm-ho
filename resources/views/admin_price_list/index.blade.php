@extends('layouts.admin')

@section('title', $title)
@section('page_title', $title)

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel" style="height:600px;">
            <a href="{{ route('admin.catalog_category.create') }}" type="button" class="btn btn-sm btn-success">Добавить категорию</a>
            <a href="{{ route('admin.priceList.upload') }}" type="button" class="btn btn-sm btn-info">Загрузать файл для скачивания</a>
            @if($pricelist)<a href="{{ route('price.download') }}">Текущий прайс "{{ $pricelist->name }}" (скачать)</a>@endif
            @include('admin_catalog_category.include.items_list')
        </div>
    </div>
@endsection