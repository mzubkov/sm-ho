<div class="x_content">
    <div class="table-responsive">
        <table class="table table-striped jambo_table bulk_action">
            <thead>
            <tr class="headings">
                <th class="column-title">Идентификатор </th>
                <th class="column-title">Название </th>
                <th class="column-title">Значение </th>
                <th class="column-title">Действия </th>
            </tr>
            </thead>

            <tbody>
            @foreach($settings as $item)
            <tr class="">
                <td class=" ">{{ $item->identifier }}</td>
                <td class=" ">{{ $item->name }}</td>
                <td class=" ">{{ $item->value }}</td>
                <td class=" last">
                    <div class="btn-group">
                        <a href="{{ route('admin.settings.edit', ['id' => $item->id]) }}" class="btn btn-xs btn-default btn-warning" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Редактировать"><i class="fa fa-edit"></i></a>
                    </div>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>