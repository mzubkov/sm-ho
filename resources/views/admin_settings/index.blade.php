@extends('layouts.admin')

@section('title', $title)
@section('page_title', $title)

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel" style="height:600px;">
            @include('admin_settings.include.items_list')
        </div>
    </div>
@endsection