<div class="col-md-3 left_col">
    <div class="left_col scroll-view">

        <div class="navbar nav_title" style="border: 0;">
            <a href="/admin" class="site_title"><i class="fa fa-home"></i> <span>Sh-ho.ru</span></a>
        </div>
        <div class="clearfix"></div>

        <br />

        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

            <div class="menu_section">
                <ul class="nav side-menu">
                    @foreach($menu as $item)
                        <li class="{{ $item['class'] }}">
                            <a href="{{ $item['url'] }}">
                                <i class="fa fa-{{ $item['icon'] }}"></i> {{ $item['title'] }}
                                @if(isset($item['items']))<span class="fa fa-chevron-down"></span>@endif
                            </a>
                            @if(isset($item['items']))
                                <ul class="nav child_menu" style="display: block;">
                                    @foreach($item['items'] as $child)
                                        <li class="{{ !empty($child['class']) ? $child['class'] : '' }}"><a href="{{ $child['url'] }}">{{ $child['title'] }}</a></li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>