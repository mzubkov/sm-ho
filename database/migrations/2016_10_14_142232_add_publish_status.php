<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPublishStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('catalog_category', function(Blueprint $table) {
            $table->tinyInteger('is_published')->default(0)->after('name');
        });

        Schema::table('catalog_product', function(Blueprint $table) {
            $table->tinyInteger('is_published')->default(0)->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('catalog_category', function(Blueprint $table) {
            $table->dropColumn('is_published');
        });

        Schema::table('catalog_product', function(Blueprint $table) {
            $table->dropColumn('is_published');
        });
    }
}
