<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            [':identifier' => 'site_title', ':name' => 'Название сайта', ':value' => 'Smart-house: товары для дома и офиса'],
            [':identifier' => 'email', ':name' => 'E-mail', ':value' => 'smarthouse2015@mail.ru'],
            [':identifier' => 'phone', ':name' => 'Телефон', ':value' => '+7 (495) 678-05-23']
        ];
        $conn = Schema::getConnection();
        foreach ($data as $row) {
            $conn->insert("INSERT INTO settings(identifier, name, value) VALUES(:identifier, :name, :value)", $row);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
