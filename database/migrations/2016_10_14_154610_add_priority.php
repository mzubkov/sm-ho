<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriority extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('catalog_category', function(Blueprint $table) {
            $table->tinyInteger('priority')->default(0)->after('is_published');
        });

        Schema::table('catalog_product', function(Blueprint $table) {
            $table->tinyInteger('priority')->default(0)->after('is_published');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('catalog_category', function(Blueprint $table) {
            $table->dropColumn('priority');
        });

        Schema::table('catalog_product', function(Blueprint $table) {
            $table->dropColumn('priority');
        });
    }
}
